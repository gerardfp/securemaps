package com.example.securemaps.Reportar;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Reporte {
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String usuario;
    public String calle;
    public double latitude;
    public double longitude;
    public String comentario;

    public Reporte(String usuario, String calle, double latitude, double longitude, String comentario) {
        this.usuario = usuario;
        this.calle = calle;
        this.latitude = latitude;
        this.longitude = longitude;
        this.comentario = comentario;
    }
}
