package com.example.securemaps;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.securemaps.Configuracion.ConfiguracionFragment;
import com.example.securemaps.Login.LoginViewModel;

public class Utils {

    // Devuelve un alert dialog basico
    public AlertDialog.Builder alertDialog(FragmentActivity activity, String titulo, String texto, String boton) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
        alertDialog.setTitle(titulo);
        alertDialog.setMessage(texto);
        alertDialog.setNegativeButton(boton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Click en este boton no se hace nada
            }
        });
        return alertDialog;
    }


    // Retorna la transicion del dialog modificar
    public FragmentTransaction fragmentTransaction(ConfiguracionFragment configuracionFragment) {
        FragmentTransaction ft = configuracionFragment.getFragmentManager().beginTransaction();
        Fragment prev = configuracionFragment.getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        return ft;
    }

    public void solicitarPermisos(Context context, FragmentActivity activity) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 101);
        }
    }

    // Funcion que obtiene el nombre de usuario de la sesion actual
    public String obtenerUsername() {
        String usuario = null;
        if (LoginViewModel.userActual.getValue() != null) {
            usuario = LoginViewModel.userActual.getValue().username;
        } else {
            usuario = "anonimo";
        }
        return usuario;
    }

}
