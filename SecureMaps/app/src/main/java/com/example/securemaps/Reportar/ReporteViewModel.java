package com.example.securemaps.Reportar;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

public class ReporteViewModel extends AndroidViewModel {
    private ReporteManager reporteManager;

    MutableLiveData<Boolean> insertResult = new MutableLiveData<>();
    public MutableLiveData<Boolean> eliminadoResult = new MutableLiveData<>();
    public MutableLiveData<Boolean> cambiarBotonCrearReportes = new MutableLiveData<>();
    public MutableLiveData<Boolean> modificadoResult = new MutableLiveData<>();

    public ReporteViewModel(Application application) {
        super(application);
        reporteManager = new ReporteManager(application);
    }

    // Insertar datos SIN COMENTARIOS
    void insertarDatos(Reporte reporte) {
        reporteManager.insertarDatos(reporte, new ReporteManager.InsertarDatosCallback() {
            @Override
            public void insertOk() {
                insertResult.postValue(true);
            }

            @Override
            public void insertError() {
                insertResult.postValue(false);
            }
        });
    }

    // Obtiene una lista con los reportes
    public LiveData<List<Reporte>> obtenerReportes(String usuario) {
        return reporteManager.obtenerReportes(usuario);
    }

    // Eliminar reporte
    public void eliminar(Reporte reporte) {
        reporteManager.eliminar(reporte, new ReporteManager.EliminarReporteCallback() {
            @Override
            public void eliminadoOk() {
                eliminadoResult.postValue(true);
            }

            @Override
            public void eliminadoError() {
                eliminadoResult.postValue(false);
            }
        });
    }

    // Modifica los datos de un reporte
    public void modificarReporte(Reporte reporte) {
        reporteManager.modificarReporte(reporte, new ReporteManager.ModificarReporteCallback() {
            @Override
            public void modificadoOk() {
                modificadoResult.postValue(true);
            }

            @Override
            public void modificadoError() {
                modificadoResult.postValue(false);
            }
        });
    }

    // Retorna un mutableLivedata vacio
    public MutableLiveData resetMutableLiveData() {
        return new MutableLiveData();
    }
}
