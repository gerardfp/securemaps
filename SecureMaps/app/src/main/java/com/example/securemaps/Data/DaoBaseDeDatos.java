package com.example.securemaps.Data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.securemaps.Login.User;
import com.example.securemaps.Reportar.Reporte;

import java.util.List;


// Gestiona los datos y demas de la base de datos
@Dao
public interface DaoBaseDeDatos {
    // Busca el usuario y password
    @Query("SELECT * FROM User WHERE username = :username AND password = :password LIMIT 1")
    User obtenerUser(String username, String password);

    // Inserta datos
    @Insert
    void insertar(User user);

    // Comprueba si existe algun usuario con ese nombre
    @Query("SELECT * FROM User where username = :username LIMIT 1")
    User comprobarUsuario(String username);

    // Cambia el usuario
    @Update
    void updateUsuario(User user);

    // Cambia la contraseña
    @Update
    void updatePassword(User user);

    // Inserta un nuevo reporte
    @Insert
    void insertarReporte(Reporte reporte);

    // Comprueba si el reporte con las siguientes coordenadas existe
    @Query("SELECT * FROM Reporte where latitude = :latitude AND longitude = :longitude AND usuario = :usuario LIMIT 1")
    Reporte comprobarReporte(double latitude, double longitude, String usuario);

    // Obtiene todos los reportes
    @Query("SELECT * FROM Reporte where usuario = :usuario")
    LiveData<List<Reporte>>  obtenerReportes(String usuario);

    @Delete
    void eliminarReporte(Reporte reporte);

    @Update
    void modificarReporte(Reporte reporte);

}
