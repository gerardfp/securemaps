package com.example.securemaps.Data;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Database;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.Update;

import com.example.securemaps.Login.User;
import com.example.securemaps.Reportar.Reporte;

import java.util.List;

@Database(entities = {User.class, Reporte.class}, version = 3, exportSchema = false)
public abstract class BaseDeDatos extends RoomDatabase{
    private static volatile BaseDeDatos INSTANCIA;

    public abstract DaoBaseDeDatos daoBaseDeDatos();


    public static BaseDeDatos obtenerInstancia(final Context context) {
        if (INSTANCIA == null) {
            synchronized (BaseDeDatos.class) {
                if (INSTANCIA == null) {
                    INSTANCIA = Room.databaseBuilder(context,
                            BaseDeDatos.class, "securemaps.db")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCIA;
    }

}
