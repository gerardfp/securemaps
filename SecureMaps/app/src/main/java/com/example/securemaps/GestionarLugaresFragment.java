package com.example.securemaps;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.securemaps.databinding.FragmentGestionarLugaresBinding;


public class GestionarLugaresFragment extends Fragment {
    FragmentGestionarLugaresBinding binding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.itemGestionarLugares);
        return (binding = FragmentGestionarLugaresBinding.inflate(inflater, container, false)).getRoot();
    }
}