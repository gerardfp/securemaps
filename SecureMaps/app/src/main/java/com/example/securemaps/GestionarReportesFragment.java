package com.example.securemaps;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.securemaps.Dialog.DialogConfirmar.DataDialogConfirmar;
import com.example.securemaps.Dialog.DialogConfirmar.DialogConfirmarFragment;
import com.example.securemaps.Dialog.DialogViewModel;
import com.example.securemaps.Reportar.MapaPrincipalFragment;
import com.example.securemaps.Reportar.Reporte;
import com.example.securemaps.Reportar.ReporteViewModel;
import com.example.securemaps.databinding.FragmentGestionarReportesBinding;
import com.example.securemaps.databinding.ViewholderReportesBinding;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

public class GestionarReportesFragment extends Fragment {
    FragmentGestionarReportesBinding binding;
    private ReporteViewModel reporteViewModel;
    private DialogViewModel dialogViewModel;
    private DialogConfirmarFragment dialogConfirmarFragment;
    private MapaPrincipalFragment mapaPrincipalFragment;
    private String usuario;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.itemGestionarReportes);
        // Inflate the layout for this fragment
        return (binding = FragmentGestionarReportesBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dialogViewModel = new ViewModelProvider(requireActivity()).get(DialogViewModel.class);
        reporteViewModel = new ViewModelProvider(requireActivity()).get(ReporteViewModel.class);
        // Pongo el recyclerview
        ReportesAdaptaer reportesAdaptaer = new ReportesAdaptaer();
        binding.listaReportes.setAdapter(reportesAdaptaer);
        usuario = new Utils().obtenerUsername();
        // Observo la lista de reportes y actualizo el recyclerview
        reporteViewModel.obtenerReportes(usuario).observe(getViewLifecycleOwner(), listaReportes -> {
            reportesAdaptaer.setReporteList(listaReportes);
        });

        usuario = new Utils().obtenerUsername();

        // Observo si se ha eliminado el reporte
        reporteViewModel.eliminadoResult.observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    Toast.makeText(getContext(), getString(R.string.reporte_eliminado), Toast.LENGTH_LONG).show();
                    reporteViewModel.eliminadoResult = reporteViewModel.resetMutableLiveData();
                } else {
                    Toast.makeText(getContext(), getString(R.string.reporte_no_eliminado), Toast.LENGTH_LONG).show();
                    reporteViewModel.eliminadoResult = reporteViewModel.resetMutableLiveData();
                }
            }
        });


        // Observo si se ha modificado el reporte
        reporteViewModel.modificadoResult.observe(getViewLifecycleOwner(), modificado -> {
            if (modificado) {
                Toast.makeText(getContext(), getString(R.string.reporte_modificado), Toast.LENGTH_LONG).show();
                reporteViewModel.modificadoResult = reporteViewModel.resetMutableLiveData();
            } else {
                Toast.makeText(getContext(), getString(R.string.error_modificar_reporte), Toast.LENGTH_LONG).show();
                reporteViewModel.modificadoResult = reporteViewModel.resetMutableLiveData();
            }
        });
    }

    // Recyclerview
    class ReportesAdaptaer extends RecyclerView.Adapter<ReportesViewHolder> {

        private List<Reporte> reporteList;

        @NonNull
        @Override
        public ReportesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ReportesViewHolder(ViewholderReportesBinding.inflate(getLayoutInflater(), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ReportesViewHolder holder, int position) {
            Reporte reporte = reporteList.get(position);
            holder.binding.texto.setText(reporte.calle);
            // Editar reporte
            holder.binding.editar.setOnClickListener(click -> {
                editarReporte(reporteList.get(position));
            });
            // Eliminar reporte
            holder.binding.eliminar.setOnClickListener(click -> {
                eliminarReporte(reporteList.get(position));
            });
        }

        @Override
        public int getItemCount() {
            if (reporteList == null) {
                return 0;
            } else {
                return reporteList.size();
            }
        }

        public void setReporteList(List<Reporte> reporteList) {
            this.reporteList = reporteList;
            notifyDataSetChanged();
        }


    }

    class ReportesViewHolder extends RecyclerView.ViewHolder {

        ViewholderReportesBinding binding;

        public ReportesViewHolder(ViewholderReportesBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

    }

    // Funcion para editar el reporte que le pasas
    private void editarReporte(Reporte reporteAntiguo) {
        mapaPrincipalFragment = new MapaPrincipalFragment(new MapaPrincipalFragment.ClickButtonModificarReporteInterface() {
            // Click en el boton de editar reporte
            @Override
            public void click(Marker lugarMarcado) {
                // Siempre que se haya marcado un lugar
                if (lugarMarcado != null) {
                    String calle = mapaPrincipalFragment.obtenerCalle(lugarMarcado.getPosition());
                    dialogConfirmarFragment = new DialogConfirmarFragment(new DialogConfirmarFragment.ClickDialogConfirmarCallback() {
                        @Override
                        public void clickButtonPositivo() {
                            double latitude = lugarMarcado.getPosition().latitude;
                            double longitude = lugarMarcado.getPosition().longitude;
                            Reporte reporteNuevo = new Reporte(usuario, calle, latitude, longitude, null);
                            reporteNuevo.id = reporteAntiguo.id;
                            reporteViewModel.modificarReporte(reporteNuevo);
                            dialogConfirmarFragment.dismiss();
                            mapaPrincipalFragment.dismiss();
                        }
                    });
                    // Datos que van dentro del dialog
                    DataDialogConfirmar dataDialogConfirmar = new DataDialogConfirmar(getString(R.string.modificar), getString(R.string.modificar_reporte) + "\n" + calle, getString(R.string.modificar));
                    dataDialogConfirmar.setIcon(R.drawable.configuracion_editar);
                    dataDialogConfirmar.setColorIcon(getString(R.color.verdePrincipal));
                    dataDialogConfirmar.setColorTitle(getString(R.color.verdePrincipal));
                    dataDialogConfirmar.setColorBotonPositivo(getString(R.color.verdePrincipal));
                    dialogViewModel.dataDialogConfirmar.postValue(dataDialogConfirmar);

                    dialogConfirmarFragment.show(getFragmentManager(), "dialog");
                } else {
                    new Utils().alertDialog(getActivity(), getString(R.string.error_titulo), getString(R.string.error_reportar), getString(R.string.boton_aceptar)).show();
                }
            }
        });
        mapaPrincipalFragment.show(getFragmentManager(), "Dialog");
        reporteViewModel.cambiarBotonCrearReportes.postValue(true);

    }

    // Funcion para eliminar el reporte que se le pasa
    private void eliminarReporte(Reporte reporte) {
        // Creo los datos que van dentro del dialog
        DataDialogConfirmar dataDialogConfirmar = new DataDialogConfirmar(getString(R.string.eliminar), getString(R.string.eliminar_reporte), getString(R.string.eliminar));
        dataDialogConfirmar.setColorTitle(getString(R.color.rojo));
        dataDialogConfirmar.setColorBotonPositivo(getString(R.color.rojo));
        dataDialogConfirmar.setIcon(R.drawable.eliminar);
        dataDialogConfirmar.setColorIcon(getString(R.color.rojo));
        dialogViewModel.dataDialogConfirmar.postValue(dataDialogConfirmar);

        dialogConfirmarFragment = new DialogConfirmarFragment(new DialogConfirmarFragment.ClickDialogConfirmarCallback() {
            @Override
            public void clickButtonPositivo() {
                reporteViewModel.eliminar(reporte);
                dialogConfirmarFragment.dismiss();
            }
        });

        dialogConfirmarFragment.show(getFragmentManager(), "Dialog");
    }
}