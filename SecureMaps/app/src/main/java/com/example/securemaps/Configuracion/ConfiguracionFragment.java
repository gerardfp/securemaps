package com.example.securemaps.Configuracion;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.example.securemaps.Dialog.DialogModificar.DataDialogModificar;
import com.example.securemaps.Dialog.DialogModificar.DialogModificarFragment;
import com.example.securemaps.Dialog.DialogViewModel;
import com.example.securemaps.Login.LoginViewModel;
import com.example.securemaps.Login.User;
import com.example.securemaps.R;
import com.example.securemaps.Utils;
import com.example.securemaps.databinding.FragmentConfiguracionBinding;

public class ConfiguracionFragment extends Fragment {
    private FragmentConfiguracionBinding binding;
    private LoginViewModel loginViewModel;
    private User user;
    private DialogViewModel dialogViewModel;
    private DialogModificarFragment dialogModificarFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.itemConfiguracion);
        return (binding = FragmentConfiguracionBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loginViewModel = new ViewModelProvider(requireActivity()).get(LoginViewModel.class);
        dialogViewModel = new ViewModelProvider(requireActivity()).get(DialogViewModel.class);
        // Lo marco como clickado
        binding.switchNotificarIncidentes.setChecked(true);
        // Oculto el password
        binding.textViewPassword.setTransformationMethod(new PasswordTransformationMethod());
        // Cambiar usuario
        binding.buttonCambiarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Compruebo si se ha logeado como anonimo o no
                if (loginViewModel.userActual.getValue() != null) {
                    cambiarUsuario();
                } else {
                    new Utils().alertDialog(getActivity(), getString(R.string.error_titulo), getString(R.string.funcion_no_disponible_para_anonimo), getString(R.string.boton_aceptar)).show();
                }
            }
        });

        // Click en el boton de ver contraseña
        binding.buttonVerPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loginViewModel.userActual.getValue() != null) {
                    loginViewModel.mostrarPassword.setValue(true);
                } else {
                    new Utils().alertDialog(getActivity(), getString(R.string.error_titulo), getString(R.string.funcion_no_disponible_para_anonimo), getString(R.string.boton_aceptar)).show();
                }
            }
        });


        // Click en el boton ocultar contraseña
        binding.buttonOcultarPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loginViewModel.userActual.getValue() != null) {
                    loginViewModel.mostrarPassword.setValue(false);
                } else {
                    new Utils().alertDialog(getActivity(), getString(R.string.error_titulo), getString(R.string.funcion_no_disponible_para_anonimo), getString(R.string.boton_aceptar)).show();
                }
            }
        });

        // Click boton cambiar password
        binding.buttonCambiarPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loginViewModel.userActual.getValue() != null) {
                    cambiarPassword();
                } else {
                    new Utils().alertDialog(getActivity(), getString(R.string.error_titulo), getString(R.string.funcion_no_disponible_para_anonimo), getString(R.string.boton_aceptar)).show();
                }
            }
        });

        // Cuando se le da click al switch se cambia el linear del correo
        binding.switchNotificarIncidentes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                clickCheck(isChecked);
            }
        });


        // Cuando cambia el usuario actualizo los textview
        loginViewModel.userActual.observe(getViewLifecycleOwner(), new Observer<User>() {
            @Override
            public void onChanged(User user) {
                // Actualizo la variable de user
                ConfiguracionFragment.this.user = user;
                binding.textViewUsuario.setText(user.username);
                binding.textViewPassword.setText(user.password);
            }
        });

        // Cuando cambia la variable mostrar contraseña
        loginViewModel.mostrarPassword.observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    mostrarContrasenya();
                } else {
                    ocultarContrasenya();
                }
            }
        });

        // Observa si se ha cambiado el usuario
        loginViewModel.updateUsuario.observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    Toast.makeText(getContext(), getString(R.string.usuario_actualizado), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getContext(), getString(R.string.error_actualizar_usuario), Toast.LENGTH_LONG).show();
                }
            }
        });


        // Observa cuando cambia la variable update password
        loginViewModel.updatePassword.observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    Toast.makeText(getContext(), getString(R.string.password_actualizado), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getContext(), getString(R.string.error_actualizar_password), Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    // Cambiar usuario
    private void cambiarUsuario() {
        // Transicion del dialogo
        FragmentTransaction ft = new Utils().fragmentTransaction(this);
        // Creo el alert dialog
        dialogModificarFragment = new DialogModificarFragment(new DialogModificarFragment.ClickDialogModificarCallback() {
            // Click boton positivo
            @Override
            public void clickButtonPositivo(String textoEditText) {
                if (!textoEditText.equals("")) {
                    user.username = textoEditText;
                    loginViewModel.updateUsuario(user, user.username);
                    dialogModificarFragment.dismiss();
                }
            }

        });

        dialogModificarFragment.show(ft, "dialog");
        // Los datos del dialog
        DataDialogModificar dataDialogModificar = new DataDialogModificar(getString(R.string.modificar_usuario_titulo), getString(R.string.modificar_usuario), getString(R.string.modificar));
        dataDialogModificar.setColorBotonPositivo(getString(R.color.verdePrincipal));
        dialogViewModel.dataDialogModificar.postValue(dataDialogModificar);

    }


    private void mostrarContrasenya() {
        // Cambio los botones
        binding.buttonVerPassword.setVisibility(View.GONE);
        binding.buttonOcultarPassword.setVisibility(View.VISIBLE);
        // Muestro el password
        binding.textViewPassword.setTransformationMethod(null);
    }


    private void ocultarContrasenya() {
        // Cambio los botones
        binding.buttonOcultarPassword.setVisibility(View.GONE);
        binding.buttonVerPassword.setVisibility(View.VISIBLE);
        // Oculto la contraseña
        binding.textViewPassword.setTransformationMethod(new PasswordTransformationMethod());
    }

    // Cambiar contraseña
    private void cambiarPassword() {
        // Transicion del dialogo
        FragmentTransaction ft = new Utils().fragmentTransaction(this);
        // Creo el alert dialog
        dialogModificarFragment = new DialogModificarFragment(new DialogModificarFragment.ClickDialogModificarCallback() {
            // Click boton positivo
            @Override
            public void clickButtonPositivo(String textoEditText) {
                if (!textoEditText.equals("")) {
                    user.password = textoEditText;
                    loginViewModel.updatePassword(user);
                }
            }
        });

        dialogModificarFragment.show(ft, "dialog");
        // Los datos del dialog
        DataDialogModificar dataDialogModificar = new DataDialogModificar(getString(R.string.modificar_password_titulo), getString(R.string.modificar_password), getString(R.string.modificar));
        dataDialogModificar.setColorBotonPositivo(getString(R.color.verdePrincipal));
        dialogViewModel.dataDialogModificar.postValue(dataDialogModificar);
    }

    // Click en el check de enviar correo
    private void clickCheck(boolean isChecked) {
        // Si esta clickado nuestro el linear
        if (isChecked) {
            binding.layoutDireccionCorreo.setVisibility(View.VISIBLE);
        }
        // Si no esta clickado, pongo invisible el linear
        else if (!isChecked) {
            binding.layoutDireccionCorreo.setVisibility(View.INVISIBLE);
        }
    }

}