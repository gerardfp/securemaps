 package com.example.securemaps;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.securemaps.Login.LoginViewModel;
import com.example.securemaps.Login.User;
import com.example.securemaps.databinding.ActivityMainBinding;
import com.example.securemaps.databinding.DrawerHeaderBinding;

 public class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;
    DrawerHeaderBinding drawerHeaderBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((binding = ActivityMainBinding.inflate(getLayoutInflater())).getRoot());
        drawerHeaderBinding = DrawerHeaderBinding.bind(binding.navView.getHeaderView(0));
        setSupportActionBar(binding.toolbar);
        // Los fragments que meto aqui, saldra el boton de hamburguesa
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.mapaPrincipalFragment, R.id.gestionarReportesFragment,
                R.id.gestionarLugaresFragment, R.id.configuracionFragment)
                .setOpenableLayout(binding.drawerLayout)
                .build();

        NavController navController = ((NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment)).getNavController();
        NavigationUI.setupWithNavController(binding.navView, navController);
        NavigationUI.setupWithNavController(binding.toolbar, navController, appBarConfiguration);

        LoginViewModel loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        // Para cambiar el usuario en el drawer
        loginViewModel.userActual.observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                drawerHeaderBinding.usuarioDrawer.setText(user.username);
            }
        });

    }
}