package com.example.securemaps.Reportar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.securemaps.Dialog.DialogReportar.DialogReportarFragment;
import com.example.securemaps.Dialog.DialogViewModel;
import com.example.securemaps.R;
import com.example.securemaps.Utils;
import com.example.securemaps.databinding.FragmentMapaPrincipalBinding;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapaPrincipalFragment extends DialogFragment implements OnMapReadyCallback {
    private FragmentMapaPrincipalBinding binding;
    private DialogViewModel dialogViewModel;
    private GoogleMap nmap;
    private Marker lugarMarcado;
    private SearchView searchView;
    private boolean primeravez;
    private ReporteViewModel reporteViewModel;
    private String usuario;
    private DialogReportarFragment dialogReportarFragment;
    private ClickButtonModificarReporteInterface clickButtonModificarReporteInterface;

    public interface ClickButtonModificarReporteInterface {
        void click(Marker lugarMarcado);
    }

    public MapaPrincipalFragment() {}

    // Constructor
    public MapaPrincipalFragment(ClickButtonModificarReporteInterface clickButtonModificarReporteInterface) {
        this.clickButtonModificarReporteInterface = clickButtonModificarReporteInterface;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Muestro la barra superior
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        // Cambio el titulo de la barra superior
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.title);
        return (binding = FragmentMapaPrincipalBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        primeravez = true;
        searchView = binding.editTextBuscar;
        dialogViewModel = new ViewModelProvider(requireActivity()).get(DialogViewModel.class);
        lugarMarcado = null;
        reporteViewModel = new ViewModelProvider(requireActivity()).get(ReporteViewModel.class);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        // Obtengo el nombre del usuario de la sesion actual
        usuario = new Utils().obtenerUsername();

        // Esto es para que funcione el searchview
        searchView.setQueryHint(getString(R.string.hind_editText));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String location = searchView.getQuery().toString();
                List<Address> addressList = null;
                if (location != null || !location.equals("")) {
                    Geocoder geocoder = new Geocoder(getContext());
                    try {
                        addressList = geocoder.getFromLocationName(location, 1);
                        // Siempre que se haya encontrado el lugar
                        if (addressList.size() > 0) {
                            Address address = addressList.get(0);
                            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                            nmap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        } else {
                            Toast.makeText(getContext(), R.string.lugar_no_encontrado, Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        // Click en nuevo reporte
        binding.floatingButtonCrearReporte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Si se ha seleccionado un lugar, creo el reporte
                if (lugarMarcado != null) {
                    crearReporte();
                }
                // Si no hay lugar seleccionado, muestro un mensaje de error
                else {
                    new Utils().alertDialog(getActivity(), getString(R.string.error_titulo), getString(R.string.error_reportar), getString(R.string.boton_aceptar)).show();
                }
            }
        });

        // Click en el boton de modificar el lugar reportado
        binding.floatingButtonModificarReporte.setOnClickListener(click -> {
            clickButtonModificarReporteInterface.click(lugarMarcado);
        });

    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        nmap = googleMap;
        // Error de permisos
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            new Utils().alertDialog(getActivity(), getString(R.string.error_titulo), getString(R.string.error_permisos_ubicacion), "").setPositiveButton(getString(R.string.boton_aceptar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getActivity().finish();
                    System.exit(0);
                }
            }).show();
            return;
        }
        // Obtengo la ubicacion actual y actualizo el mapa
        nmap.setMyLocationEnabled(true);
        actualizarPosicion(nmap);
        // Obtengo los lugares reportados
        reporteViewModel.obtenerReportes(usuario);

        // Click en el mapa
        nmap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                // Si es la primera vez que se clicka
                if (lugarMarcado == null) {
                    MarkerOptions a = new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
                    lugarMarcado = nmap.addMarker(a);
                }
                // Cambio de coordenadas el marcador
                lugarMarcado.setPosition(latLng);
            }
        });

        // Observo la lista de reportes y los actualizo
        reporteViewModel.obtenerReportes(usuario).observe(getViewLifecycleOwner(), new Observer<List<Reporte>>() {
            @Override
            public void onChanged(List<Reporte> reportes) {
                for (Reporte reporte : reportes) {
                    // Marco cada lugar reportado en el mapa
                    LatLng latLng = new LatLng(reporte.latitude, reporte.longitude);
                    nmap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                }
            }
        });

        // Observo la variables de datos insertados
        reporteViewModel.insertResult.observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                // Datos insertados
                if (aBoolean) {
                    Toast.makeText(getContext(), getString(R.string.reporte_insertado), Toast.LENGTH_LONG).show();
                    reporteViewModel.insertResult = reporteViewModel.resetMutableLiveData();
                }

                // Datos no insertados
                else {
                    Toast.makeText(getContext(), getString(R.string.error_registrar_reporte), Toast.LENGTH_LONG).show();
                    reporteViewModel.insertResult = reporteViewModel.resetMutableLiveData();
                }
            }
        });

        // Observo la variable de si hay que cambiar el floating button
        reporteViewModel.cambiarBotonCrearReportes.observe(getViewLifecycleOwner(), cambiarFloatingCrearReportes -> {
            // Cambia el boton de crear por el de modificar
            if (cambiarFloatingCrearReportes) {
                binding.floatingButtonCrearReporte.setVisibility(View.GONE);
                binding.floatingButtonModificarReporte.setVisibility(View.VISIBLE);
            }
            // Cambia el boton de modificar por el de crear
            else {
                binding.floatingButtonModificarReporte.setVisibility(View.GONE);
                binding.floatingButtonCrearReporte.setVisibility(View.VISIBLE);
            }
        });

    }

    // Obtiene la calle de las cordenadas que le pasas
    public String obtenerCalle(LatLng latLng) {
        String calle = null;
        Geocoder gc = new Geocoder(getContext(), Locale.getDefault());
        try {
            List<Address> addresses = gc.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses.size() > 0) {
                calle = addresses.get(0).getThoroughfare();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return calle;
    }

    // Actualiza la posicion actual en el mapa
    @SuppressLint("MissingPermission")
    private void actualizarPosicion(GoogleMap nmap) {
        nmap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                // Solo muevo el mapa la primera vez
                if (primeravez) {
                    nmap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15));
                    primeravez = false;
                }
            }
        });
    }

    // Boton para crear un nuevo reporte
    private void crearReporte() {
        String calle = obtenerCalle(lugarMarcado.getPosition());
        dialogReportarFragment = new DialogReportarFragment(calle, new DialogReportarFragment.ClickDialogReportarCallback() {
            // Click boton reportar
            @Override
            public void clickButtonReportar(String comentario) {
                double latitude = lugarMarcado.getPosition().latitude;
                double longitude = lugarMarcado.getPosition().longitude;
                Reporte reporte = new Reporte(usuario, calle, latitude, longitude, comentario);
                // Inserto los datos
                reporteViewModel.insertarDatos(reporte);
                dialogReportarFragment.dismiss();
            }
        });
        dialogReportarFragment.show(getChildFragmentManager(), "dialog");
    }
}