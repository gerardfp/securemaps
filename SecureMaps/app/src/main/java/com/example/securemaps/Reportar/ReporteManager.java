package com.example.securemaps.Reportar;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.securemaps.Data.BaseDeDatos;
import com.example.securemaps.Data.DaoBaseDeDatos;
import com.example.securemaps.Login.LoginViewModel;
import com.example.securemaps.R;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ReporteManager {
    private DaoBaseDeDatos daoBaseDeDatos;
    private Executor executor = Executors.newSingleThreadExecutor();

    interface InsertarDatosCallback {
        void insertOk();
        void insertError();
    }

    public interface EliminarReporteCallback {
        void eliminadoOk();
        void eliminadoError();
    }

    interface ModificarReporteCallback{
        void modificadoOk();
        void modificadoError();
    }

    ReporteManager(Application application) {
        daoBaseDeDatos = BaseDeDatos.obtenerInstancia(application).daoBaseDeDatos();
    }

    // Inserta datos
    void insertarDatos(Reporte reporte, InsertarDatosCallback insertarDatosCallback) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                daoBaseDeDatos.insertarReporte(reporte);
                // Compruebo si se ha insertado
                if (daoBaseDeDatos.comprobarReporte(reporte.latitude, reporte.longitude, reporte.usuario) != null) {
                    insertarDatosCallback.insertOk();
                }
                // No existe el reporte insertado
                else {
                    insertarDatosCallback.insertError();
                }
            }
        });
    }

    // Devuelve una lista de todos los reportes
    LiveData<List<Reporte>> obtenerReportes(String usuario) {
        return daoBaseDeDatos.obtenerReportes(usuario);
    }

    // Elimina un reporte
    void eliminar(Reporte reporte, EliminarReporteCallback eliminarReporteCallback) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                daoBaseDeDatos.eliminarReporte(reporte);
                // Compruebo si se ha eliminado
                if (daoBaseDeDatos.comprobarReporte(reporte.latitude, reporte.longitude, reporte.usuario) == null) {
                    eliminarReporteCallback.eliminadoOk();
                } else {
                    eliminarReporteCallback.eliminadoError();
                }

            }
        });
    }

    void modificarReporte(Reporte reporte, ModificarReporteCallback modificarReporteCallback) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                daoBaseDeDatos.modificarReporte(reporte);
                // Compruebo si existe, una vez modificado
                if (daoBaseDeDatos.comprobarReporte(reporte.latitude, reporte.longitude, reporte.usuario) != null) {
                    modificarReporteCallback.modificadoOk();
                } else {
                    modificarReporteCallback.modificadoError();
                }
            }
        });
    }

}
